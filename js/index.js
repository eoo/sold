/*===scroll===*/
(function(){
	$(document).ready(function() {
	    $('.arrow').on('click', function(e) {
	    	e.preventDefault();
	    	if (e.target && e.target.nodeName === 'A') {
	    	    var item = $(e.target).attr('href');
	    	    $.scrollTo(item, 1000);
	    	 }        
	    });
	});
})();

(function(){
	$(document).ready(function() {
		var height = $(window).height();
		var contentInfoPageHeight = parseInt($('.content-info-page').css('height'));
		if (contentInfoPageHeight < (height - 360)) {
			$('.content-info-page').css('height', (height - 360));
		}
		var contentListPageHeight = parseInt($('.content-list-page').css('height'));
		if (contentListPageHeight < (height - 360)) {
			$('.content-list-page').css('height', (height - 360));
		}
		var contentHeight = parseInt($('.content').css('height'));
		if (contentHeight < (height - 360)) {
			$('.content').css('height', (height - 360));
		}
		var newsPageHeight = parseInt($('.news-page').css('height'));
		if (newsPageHeight < (height - 360)) {
			$('.news-page').css('height', (height - 360));
		}

		
		var topOffset = (height - 300)/2 - 45;
		var topOffset2 = (height - 575)/2 - 30;
		if (height > 750) {
			$('.slide-block').css('height', height);
			$('.slide-block .info').css('top', topOffset);
			$('#system .info').css('top', topOffset2);
		}
		if (height > 1000) {
			$('.memorial-img').css('top', height*0.1);
		}
	});
})();

function parallax(){
    var scrolled = $(window).scrollTop();
    $('.bg').css('top', -(scrolled * 0.4) + 'px');
}
$(window).scroll(function(e){
    parallax();
});

// var windowHeightOffset = $(window).height()*0.7;

// var topArray = $('#memorial .layer-1').map(function(index) {
//     return parseInt($(this).css('top'));
// });
// var topArray2 = $('#names .layer-1').map(function(index) {
//     return parseInt($(this).css('top'));
// });
// var topArray3 = $('#smile .layer-1').map(function(index) {
//     return parseInt($(this).css('top'));
// });
// var topArray4 = $('#memorial .layer-2').map(function(index) {
//     return parseInt($(this).css('top'));
// });
// var topArray5 = $('#names .layer-2').map(function(index) {
//     return parseInt($(this).css('top'));
// });
// var topArray6 = $('#smile .layer-2').map(function(index) {
//     return parseInt($(this).css('top'));
// });
// var topArray7 = $('#memorial .layer-3').map(function(index) {
//     return parseInt($(this).css('top'));
// });
// var topArray8 = $('#names .layer-3').map(function(index) {
//     return parseInt($(this).css('top'));
// });
// var topArray9 = $('#smile .layer-3').map(function(index) {
//     return parseInt($(this).css('top'));
// });
 

// function parallax2() {
//     var scrolled = $(window).scrollTop();
//     $('#memorial .layer-1').each(function(index) {
//         $(this).css('top', (topArray[index] - scrolled * 0.4));
//     });
//     $('#names .layer-1').each(function(index) {
//         $(this).css('top', (topArray2[index] - scrolled * 0.4 + windowHeightOffset));
//     });
//     $('#smile .layer-1').each(function(index) {
//         $(this).css('top', (topArray3[index] - scrolled * 0.4 + windowHeightOffset * 2));
//     });
//     $('#memorial .layer-2').each(function(index) {
//         $(this).css('top', (topArray4[index] - scrolled * 0.25));
//     });
//     $('#names .layer-2').each(function(index) {
//         $(this).css('top', (topArray5[index] - scrolled * 0.25 + windowHeightOffset * 0.4));
//     });
//     $('#smile .layer-2').each(function(index) {
//         $(this).css('top', (topArray6[index] - scrolled * 0.25 + windowHeightOffset * 0.4 * 2));
//     });
//     $('#memorial .layer-3').each(function(index) {
//         $(this).css('top', (topArray7[index] - scrolled * 0.1));
//     });
//     $('#names .layer-3').each(function(index) {
//         $(this).css('top', (topArray8[index] - scrolled * 0.1 + windowHeightOffset * 0.1));
//     });
//     $('#smile .layer-3').each(function(index) {
//         $(this).css('top', (topArray9[index] - scrolled * 0.1 + windowHeightOffset * 0.1 * 2));
//     });
// }






